console.log(`S18 Activity`);


//Activity Part 1: Objects
let trainer = {
	name: `Ash Ketchum`,
	age: `10`,
	pokemon: [
		`Pikachu`,
		`Charizard`,
		`Squirtle`,
		`Bulbasaur`
	],
	friends: 
		{kanto: [`Brock`,`Misty`],
		hoenn: [`May`, `Max`]
		},
	talk: function(){
		console.log(`Pikachu! I choose you!`);
	}
};

console.log(trainer);
//display using dot notation and bracket
// console.log(`Welcome trainer ${trainer.name}! You are ${trainer["age"]} years old, which makes you qualified for a gym battle. Your roster is ${trainer.pokemon.join(",")}.`);
console.log(`Result of dot notation:`);
console.log(trainer.name);
console.log(`Result of bracket notation:`);
console.log(trainer["pokemon"]);
console.log(`Result of talk method`);
trainer.talk();


//Activity Part 2: Object Constructors

function Pokemon(name, lvl){
	this.name = name;
	this.level = lvl;
	//this.health = parseInt(lvl * 50);
	//this.attack = parseInt((lvl * 50)/3);
	this.health = lvl * 2;
	this.attack = lvl;
	this.tackle = function(opponent){
		opponent.health -= parseInt(this.attack);
		//console.log(`${this.name} dealt ${this.attack} damage to ${opponent.name}. Remaining health is ${opponent.health}.`);
		console.log(`${this.name} tackled ${opponent.name}.`);
		console.log(`${opponent.name}'s health is now reduced to ${opponent.health}.`);

		if (opponent.health <= 0){
			opponent.faint();	
		}
		//return opponent;
	},
	this.faint = function(){
		console.log(`${name} has fainted!`);
	}
}

//initialize
// let treecko = new Pokemon(`Grassie`,5);
// let charmander = new Pokemon(`Char`, 6);
// let piplup = new Pokemon(`Pingu`, 10);
let pikachu = new Pokemon(`Pikachu`,12);
let geodude = new Pokemon(`Geodude`,8);
let mewtwo = new Pokemon(`Mewtwo`,100);

// console.log(treecko);
// console.log(charmander);
// console.log(piplup);

// piplup.tackle(treecko);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(pikachu);
console.log(pikachu);
console.log(geodude);




